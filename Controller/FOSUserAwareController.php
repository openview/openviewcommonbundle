<?php

/**
 *
 *  This is a simple controller that provides a shortcut method to
 *  get the User from the FOSUserBundle.
 *
 *  This will also be useful in case some parameters will change
 *
 */

namespace Openview\CommonBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use Openview\CommonBundle\Controller\FOSUserAwareControllerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FOSUserAwareController extends Controller implements FOSUserAwareControllerInterface
{
    public function getCurrentUser() {
        return $this->container->get('security.context')->getToken()->getUser();
    }

    /**
     *  @param mixed $user The object to check
     *
     *  Check if the given user is an actual FOSUser
     *
     *  @return boolean Whether the given object is a user or not 
     */
    public function checkUser($user) {
        return is_object($user) && $user instanceof UserInterface;
    }
    
    public function getCurrentUserWithCheck() {
        $u = $this->getCurrentUser();
        
        if (!$this->checkUser($u)) {
            throw new \Exception('This exception means that the user returned from the security.context service is not really an user, but I don\'t know how to write it in a nice and short way.');
        }
        
        return $u;
    }

    /**
     *  Ask the security.context for the current user.
     *  If a user is returned return it, otherwise redirect to the login action.
     *
     *  @return User|Response
     */
    public function getUserOrRedirectToLogin()
    {
        $user = $this->getCurrentUser();

        if(!$this->checkUser($user)) {
            return $this->redirect($this->generateURL('fos_user_security_login'));
        }

        return $user;
    }
}
