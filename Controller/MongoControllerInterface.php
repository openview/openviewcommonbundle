<?php

namespace Openview\CommonBundle\Controller;

interface MongoControllerInterface 
{
    /**
     *	Gets the Doctrine Mongo Document Mangager
     */
    public function getDoctrineMongoDocumentManager();
}
