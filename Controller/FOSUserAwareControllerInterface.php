<?php

namespace Openview\CommonBundle\Controller;

interface FOSUserAwareControllerInterface 
{
    /**
     * Gets the current user
     */ 
    public function getCurrentUser();
    
    /**
     * Ges the current user. Thorws an exception if the it does not exists
     */
    public function getCurrentUserWithCheck();
}
