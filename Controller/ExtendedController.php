<?php

/**
 *  This is a controller with handy functionality for components that 
 *  are defacto Symfony2 standards but not yet added to the framework
 *
 *  Mongo
 *  FOSUser
 *
 */

namespace Openview\CommonBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use Openview\CommonBundle\Controller\FOSUserAwareControllerInterface;
use Openview\CommonBundle\Controller\MongoControllerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ExtendedController extends Controller implements MongoControllerInterface, FOSUserAwareControllerInterface
{
    public function getDoctrineMongoDocumentManager() {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }
    
    public function getCurrentUser() {
        return $this->container->get('security.context')->getToken()->getUser();
    }
    
    public function getCurrentUserWithCheck() {
        $u = $this->getCurrentUser();
        
        if (!is_object($u) || !$u instanceof UserInterface) {
            throw new \Exception('This user does not have access to this section.');
        }
        
        return $u;
    }
}
