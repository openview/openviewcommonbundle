<?php

/**
 *
 *  This is a simple controller that provides a shortcut method to
 *  get the Doctrine default Mongo document manager.
 *
 *  This will also be useful in case some parameters will change
 *
 */

namespace Openview\CommonBundle\Controller;

use Openview\CommonBundle\Controller\MongoControllerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MongoController extends Controller implements MongoControllerInterface
{
    public function getDoctrineMongoDocumentManager() {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }
}
