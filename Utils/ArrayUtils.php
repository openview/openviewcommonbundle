<?php

namespace Openview\CommonBundle\Utils;

/**
 *	This class contains useful snippets for array handling
 */

class ArrayUtils
{
	/**
	 *	Get the first element the array given as parameter.
	 *	Useful when the array is the result of a function call, since you cannot do
	 *	something like myMethodThatReturnsAnArray()[0]
	 */
	static function getFirstInArray($theArray)
	{
		return $theArray[0];
	}
}