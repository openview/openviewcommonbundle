OpenviewCommonBundle
====================

This Bundle aims to collect a set of useful Classes, Interfaces and Methods to simplify the development of Symfony2 applications, so that we can write more readable code and do it faster.

## TODOs

  * Implement checks that throw exceptions
  * Add some suggest and requires in the composer.json